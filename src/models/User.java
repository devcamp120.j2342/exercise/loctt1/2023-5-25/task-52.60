package models;

public class User {
     private String password;
     private String username;
     private boolean Enabled;


     public String getPassword() {
          return password;
     }
     public void setPassword(String password) {
          this.password = password;
     }
     public String getUsername() {
          return username;
     }
     public void setUsername(String username) {
          this.username = username;
     }
     public boolean isEnabled() {
          return Enabled;
     }
     public void setEnabled(boolean enabled) {
          Enabled = enabled;
     }
     public User() {
     }
     
     public User(String password, String username, boolean enabled) {
          this.password = password;
          this.username = username;
          Enabled = enabled;
     }

     
     
}
