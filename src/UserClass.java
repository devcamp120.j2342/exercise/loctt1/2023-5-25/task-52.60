import models.User;

public class UserClass {
    public static void main(String[] args) throws Exception {
        User user1 = new User();
        User user2 = new User("150117", "SinSin", false);

        System.out.println("USER 1: ");
        System.out.println(user1.toString());

        System.out.println("---------------------------------");

        System.out.println("USER 2: ");
        System.out.println(user2.toString());
    }
}
